package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdac.dao.CustomerDao;
import com.cdac.dao.CustomerDaoImpl;
import com.cdac.pojo.Customer;

@WebServlet(urlPatterns = "/authenticate", loadOnStartup = 1)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	CustomerDao customerDao = null;

	public LoginServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		System.out.println("Init Method : " + this.getClass().getName());

		/*
		 * We don't have to handle exception here only, we throw it, So that Web
		 * container should come to know that init() method has failed.
		 */
		try {
			customerDao = new CustomerDaoImpl();
			System.out.println("Initialized Customer Dao");
		} catch (ClassNotFoundException | SQLException e) {
			/*
			 * Centralized exception handling by throwing exception
			 */
			throw new ServletException("Error in CustomerDao inialization", e);
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Customer customer = null;
		try {
			customer = customerDao.authenticateCustomer(email, password);
		} catch (Exception e) {
			throw new ServletException("Error in " + this.getClass().getName(), e);
		}

		response.setContentType("text/html");
		try (PrintWriter pw = response.getWriter()) {

			if (customer == null)
				pw.println("Invalid Login. " + "<a href='login.html'>Retry</a>");
			else {

				pw.println("Login Successfull!");
				pw.println("Customer Details : " + customer);

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("books");
				requestDispatcher.forward(request, response);

				/*
				 * requestDispatcher.forward(request,response) method will clear the buffer and
				 * send/forward request-response object to other servlet
				 * 
				 * We forward request to book servlet from the loginServlet's doGet() method, so
				 * the books servlet's doGet() method will be called
				 * 
				 * If we forward request to book servlet from the loginServlet's doPost()
				 * method, so the books servlet's doPost() method will be called
				 * 
				 * Cross method support is not allowed
				 */

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void destroy() {
		try {
			customerDao.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error in destroy() " + this.getClass().getName(), e);
		}

	}

}
